import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'package:http/http.dart' as http;
import 'package:mockito_task/modiles/tapObj.dart';
import 'package:mockito_task/repositories/rolesRepositories.dart';

import 'role_test.mocks.dart';

@GenerateNiceMocks([MockSpec<TapObj>()])
@GenerateMocks([http.Client])

void main() {
  List<String> roles = ["role1", "role2"];
  int id = 1;
  String role = "role";

  group("AddRole", () {

    test("SuccessAddRole", () {
      final client = MockClient();
      when(client.post(Uri.parse('https://www.tadal.com/addrole/$id/$role')))
          .thenAnswer((_) async => http.Response(
              json.encode(TapObj(
                  id: 1,
                  name: "aaa",
                  displayName: "bbb",
                  icon: "@",
                  roles: "ccc")),
              200));

      expect(RoleRepositories(client: client).addRoleToTab(id, role),
          isA<Future<TapObj>>());
    });
    test("errorAddRole", () {
      final client = MockClient();

      when(client.post(Uri.parse('https://www.tadal.com/addrole/$id/$role')))
          .thenAnswer((_) async => http.Response('Not Found', 404));
      expect(RoleRepositories(client: client).addRoleToTab(id, role),
          throwsException);
    });
  });

  group('RemoveRole', () {
    test("successRemoveRole", () {
      final client = MockClient();

      when(client.post(
        Uri.parse('https://www.tadal.com/removerole/$id/$role'),
      )).thenAnswer((_) async => http.Response(
          jsonEncode(TapObj(
              id: 4, name: "aa", displayName: "bb", icon: "@@", roles: "cc")),
          200));

      expect(RoleRepositories(client: client).removeRoleToTap(id, role)..whenComplete(() => RoleRepositories(client: client).addRolesToTap(id, roles))  ,
          isA<Future<TapObj>>());
    });

    test('RemoveRoleError', () {
      final client = MockClient();

      when(client.post(Uri.parse('https://www.tadal.com/removerole/$id/$role')))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expectSync(RoleRepositories(client: client).removeRoleToTap(id, role),
          throwsException);
    });
  });

  group("AddRoles", ()  {

    test("SuccessAddRoles", () {
       final client = MockClient();

       when(  client.post(Uri.parse('https://www.tadal.com/addroles/$id'),
              body: {"roles": roles.toString()}))
          .thenAnswer((_)  async=>   http.Response(
              jsonEncode(TapObj(
                  id: 2,
                  name: "aaa",
                  displayName: "bbb",
                  icon: "@",
                  roles: "role1,role2")),
              200));

      expect(RoleRepositories(client: client).addRolesToTap(id, roles).whenComplete(() => RoleRepositories(client: client).removeRoleToTap(id, role)),
          isA<Future<TapObj>>());
    });
    test("errorAddRoles", () {
      final client = MockClient();

      when(client.post(Uri.parse('https://www.tadal.com/addroles/$id'),
              body: {"roles": roles.toString()}))
          .thenAnswer((_) async => http.Response('Not Found', 404));
      expect(RoleRepositories(client: client).addRolesToTap(id, roles),
          throwsException);
    });
  });

  group('RemoveRoles', () {

    final client = MockClient();
    test('returns an TapObj if the http call completes successfully', ()  {
      when(  client.post(Uri.parse('https://www.tadal.com/removeroles/$id/'),
          body: {
            "roles": roles.toString()
          })).thenAnswer((_)  async=> http.Response( json.encode(TapObj(
          id: 2,
          name: "aaa",
          displayName: "bbb",
          icon: "@",
          roles: "role1,role2")),
          200));

      expect(RoleRepositories(client: client).removeRolesToTap(id, roles)..whenComplete(() => RoleRepositories(client: client).addRolesToTap(id, roles)),
          isA<Future<TapObj>>());
    });
    test('throws an exception if the http call completes with an error', () {
      when(client.post(Uri.parse('https://www.tadal.com/removeroles/$id/'),
              body: {"roles": roles.toString()}))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect(RoleRepositories(client: client).removeRolesToTap(id, roles)..whenComplete(() => RoleRepositories(client: client).removeRolesToTap(id, roles)),
          throwsException);
    });
  });


  group('SitMap', () {      final client = MockClient();

  test('returns an TapObj if the http call completes successfully', ()  {

      when( client.post(Uri.parse('https://www.tadal.com/sitMap/')))
          .thenAnswer((_)  async=> http.Response(
              [
                '{"id": 1, "name": "aaa", "displayName": "bbb","icon":"@","roles":"ccc"}',
                '{"id": 1, "name": "aaa", "displayName": "bbb","icon":"@","roles":"ccc"}',
                '{"id": 1, "name": "aaa", "displayName": "bbb","icon":"@","roles":"ccc"}'
              ].toString(),
              200));

      expect(RoleRepositories(client: client).sitMap(roles).whenComplete(() => RoleRepositories(client: client).addRolesToTap(id, roles)),
          isA<Future<List<TapObj>>>());
    });

    test('throws an exception if the http call completes with an error', ()  {

      when(client.post(Uri.parse('https://www.tadal.com/sitMap/'),
              body: {"role": roles.toString()}))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect( RoleRepositories(client: client).sitMap(roles)..whenComplete(() => RoleRepositories(client: client).sitMap( roles)),
          throwsException);
    });
  });
}
