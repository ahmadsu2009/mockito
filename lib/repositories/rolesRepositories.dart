import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:mockito_task/modiles/tapObj.dart';

class RoleRepositories {
  final http.Client client;

  RoleRepositories({required this.client});


  Future<TapObj> addRoleToTab(int tabId ,String role)async {

    final response = await client
        .post(Uri.parse('https://www.tadal.com/addrole/$tabId/$role'));

    if (response.statusCode == 200) {

      return  TapObj.fromJson(jsonDecode(response.body));
    } else {

      throw Exception('Failed to add Role');
    }
  }

  Future<TapObj> addRolesToTap(int id, List<String> role) async {
    final response = await http.post(
      Uri.parse("https://www.tadal.com/addroles/$id"),
      body: {"roles": role.toString()},
    );

    if (response.statusCode == 200) {
      return TapObj.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to add roles');
    }
  }

  Future<TapObj> removeRoleToTap(int id, String role) async {
    final response = await http.post(
      Uri.parse("https://www.tadal.com/removerole/$id/$role"),
    );

    if (response.statusCode == 200) {
      return TapObj.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to remove Role');
    }
  }

  Future<TapObj> removeRolesToTap(int id, List<String> role) async {
    final response = await http.post(
      Uri.parse("https://www.tadal.com/removeroles/$id/"),
      body: {"role": role.toString()},
    );

    if (response.statusCode == 200) {
      return TapObj.fromJson(jsonDecode(response.body));
    } else {
      log("Failed to add role");
      throw Exception('Failed remove Roles');
    }
  }

  Future<List<TapObj>> sitMap(List<String> role) async {
    final response = await http.post(
      Uri.parse("https://www.tadal.com/sitMap/"),
      body: {"role": role.toString()},
    );

    if (response.statusCode == 200) {
      List<TapObj> tapObg= List.from(jsonDecode(response.body)).map((e)=>TapObj.fromJson(e)).toList();
      return  tapObg;
    } else {
      throw Exception('Failed to get sitMap');
    }
  }
}
