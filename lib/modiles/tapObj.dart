
class TapObj {
  final int id;
  final String name;
  final String displayName;
  final String icon;
  final String roles;

  TapObj({required this.id, required this.name, required this.displayName, required this.icon, required this.roles});



  factory TapObj.fromJson(Map<String, dynamic> json) {
    return TapObj(
      id: json["id"],
      name: json["name"],
      displayName: json["displayName"],
      icon: json["icon"],
      roles: json["roles"],
    );

  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "displayName":displayName,
      "icon":icon,
      "roles":roles,
    };}

}