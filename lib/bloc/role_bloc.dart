import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mockito_task/repositories/rolesRepositories.dart';

import '../modiles/tapObj.dart';

part 'role_event.dart';
part 'role_state.dart';

class RoleBloc extends Bloc<RoleEvent, RoleState> {
  RoleRepositories repo;

  RoleBloc(this.repo) : super(RoleInitial()) {
    on<RoleEvent>((event, emit) async{
      if (event is AddRoleEvent) {
        emit (LoadingState());

        try {
          TapObj tapObj = await repo.addRoleToTab(event.id, event.role);
          emit (SuccessAddRoleState(tapObj: tapObj));
        } catch (e) {
          log(e.toString());
          emit (ErrorState(e.toString()));
        }
      }

    });
  }
}
