part of 'role_bloc.dart';

@immutable
abstract class RoleEvent {}

class AddRoleEvent extends RoleEvent {
  final int id;
  final String role;
  AddRoleEvent({required this.id, required this.role});
}

class RemoveRoleEvent extends RoleEvent {
  final int id;
  final String role;
  RemoveRoleEvent({required this.id, required this.role});
}

class AddRolesEvent extends RoleEvent {
  final int id;
  final List<String> roles;
  AddRolesEvent({required this.id, required this.roles});
}

class RemoveRolesEvent extends RoleEvent {
  final int id;
  final List<String> roles;
  RemoveRolesEvent({required this.id, required this.roles});
}

class SitMapEvent extends RoleEvent {
  final List<String> roles;
  SitMapEvent({required this.roles});
}
