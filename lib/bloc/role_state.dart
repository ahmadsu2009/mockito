part of 'role_bloc.dart';

@immutable
abstract class RoleState {}

class RoleInitial extends RoleState {}

class LoadingState extends RoleState {}

class SuccessAddRoleState extends RoleState {
  final TapObj tapObj;

  SuccessAddRoleState({required this.tapObj});
}

class SuccessRemoveRoleState extends RoleState {
  final TapObj tapObj;

  SuccessRemoveRoleState({required this.tapObj});
}

class SuccessAddRolesState extends RoleState {
  final TapObj tapObj;

  SuccessAddRolesState({required this.tapObj});
}

class SuccessRemoveRolesState extends RoleState {
  final TapObj tapObj;

  SuccessRemoveRolesState({required this.tapObj});
}

class SuccessSitMapState extends RoleState {
  final List<TapObj> tapObj;

  SuccessSitMapState({required this.tapObj});
}

class ErrorState extends RoleState {
  final String error;
  ErrorState(this.error);
}
